#!/bin/bash

set -e

cd $(dirname $0)

REGISTRY_PREFIX="registry.gitlab.com/chriz2600"
PSN00BSDK_VERSION="0.15b"
PSXSDK_VERSION="0.0.9"

docker run -it -v $(pwd):/build ${REGISTRY_PREFIX}/psn00bsdk-docker:${PSN00BSDK_VERSION} make
docker run -it -v $(pwd):/build ${REGISTRY_PREFIX}/docker-psxsdk:${PSXSDK_VERSION} ./create-iso
