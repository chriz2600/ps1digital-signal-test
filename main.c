/* 
 * Based on RGB24 Example by Lameguy64
 * https://github.com/Lameguy64/PSn00bSDK/tree/master/examples/graphics/rgb24
 */

#include <stdio.h>
#include <ioctl.h>
#include <sys/fcntl.h>
#include <string.h>
#include <psxapi.h>
#include <psxgte.h>
#include <psxgpu.h>
#include <psxpad.h>
#include <psxsio.h>

#define TIMCOUNT 2

// So data from tim.s can be accessed
extern unsigned int tim_image0[];
extern unsigned int tim_image1[];
char pad_buff[2][34];

int main() {

	DISPENV		disp;
	TIM_IMAGE	tim;
	PADTYPE 	*pad;
	short		lastXState = false;
	short		XState = false;
	short 		usedtim = 0;

	AddSIO(115200);

	// Reset GPU
	ResetGraph(0);
	SetVideoMode(MODE_NTSC);
	SetDefDispEnv(&disp, 0, 0, 640, 480);

	disp.isrgb24 = 1;
	disp.isinter = 1;
	
	// Apply and enable display
	PutDispEnv(&disp);
	SetDispMask(1);
	
	// Upload image to VRAM
	GetTimInfo(tim_image0, &tim);
	LoadImage(tim.prect, tim.paddr);
	DrawSync(0);

	InitPAD(&pad_buff[0][0], 34, &pad_buff[1][0], 34);
	StartPAD();
	ChangeClearPAD(0);

	printf("Entering loop...\n");

	while(1) {

		pad = (PADTYPE*)&pad_buff[0][0];

		if( pad->stat == 0 ) {
			// For digital pad, dual-analog and dual-shock
			if( ( pad->type == 0x4 ) || ( pad->type == 0x5 ) || ( pad->type == 0x7 ) ) {
				lastXState = XState;
				if( !(pad->btn&PAD_TRIANGLE) ) {
					XState = true;
				} else {
					XState = false;
				}
			}
		}

		if (!lastXState && XState) {
			usedtim = usedtim < TIMCOUNT - 1 ? usedtim + 1 : 0;
			printf("trigger! %d\n", usedtim);
			switch (usedtim) {
				case 0:
					GetTimInfo(tim_image0, &tim);
					break;
				case 1:
					GetTimInfo(tim_image1, &tim);
					break;
				// case 2:
				// 	GetTimInfo(tim_image2, &tim);
				// 	break;
			}
			LoadImage(tim.prect, tim.paddr);
		}

		DrawSync(0);
		VSync(0);
	}
	
	return 0;
}